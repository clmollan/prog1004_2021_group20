# **Collaboration Agreement for Team 20**
## Members:

Alexander Kristian Damhaug  

Anders Lunde Hagen  

Sander Thorsen


Petar Ilic 


Camilla Li Dagsgård Molland 


## **Introduction**
This collaboration agreement is based on the MPRI model and designed to establish a good base for further teamwork. The agreement is built upon a collection of goals, role responsibilities, procedures, and guidelines for interaction within the team. Based on these points we all got a common understanding of how we want the teamwork to be executed. 

## **Goals**
### Effect goals
1.	To achieve a good working environment, it is important to respect,  trust and support each other.
2.	By having the roles evenly distributed where everyone feels included, and there is room to discuss, makes a solid basis for being able to cooperate effectively.
3.	To increase productivity, it’s important to motivate and engage with each other as well as ask the teaching assistants and possibly the lecturer if there’s any need.

### Result goals
1.	To make sure that all tasks are handed in on schedule, we have a goal to finish 1-2 days before the deadline expires. 
2.	Good teamwork.
3.	To produce a good end product, we are going to accomplish this through communication, teamwork and focused work. 

## **Role responsibilities**
### Project leader - **Anders**
Has the overall responsible for the project, and making sure that all team members sufficiently fulfil their roles. 
### Organisation of meetings - **Sander**
Responsible of summoning meetings and making sure that all team members get the meeting information in advance. 
### Reporter - **Camilla**
Responsible of taking notes and documenting meetings.
### Documentation responsible – **Petar**
Responsible of archiving the documents and making sure that they are available for everyone.
### Contact person - **Alexander**
Responsible for contacting teaching assistant and teacher if necessary.

## **Procedures**
### Meetings
The meetings should start and end at the decided time. And by having an agenda for each meeting, should make it easier to follow the given timeframe. The agenda should contain all important topics that needs to be addressed in fellowship, and it is created of all team members. During the meeting we will have a break to make sure that we do not lose focus. 
We decided to have weekly meetings on Monday and Wednesday from 14.00-16.00. The meetings will take place at the same time each week and will therefore do not need any summons. If we need to add or dismiss meetings, organisation of meetings responsible will inform all team members. In case of absence, we will send a message to the meeting responsible on Discord. 
### Documents
After every session, we are going to upload the documents to teams and GitLab so that everyone can access them. 
Policy for monitoring tasks
Have a meeting on Mondays every week, to make sure that everyone is up to date with their tasks. It is the project leader's overall responsibility to follow up that everyone is known with their tasks.  
### Submission of teamwork
Everyone is responsible of keeping deadlines and all should approve the other team members work before delivery. 

## **Interaction**
### Attendance and preparation
Everyone should be present at least 5 minutes before the meeting starts to make sure that all necessary documents and work-related programs are running and ready within meeting start. It is the team member own responsibility to be prepared and up to date with the latest information regarding the teamwork. 
### Presence and commitment
All team members should be focused and committed to making the project as successful as possible. During meetings it is expected that all team members got their full attention on the agenda. 
### How to support each other
By having mutual respect and trust the threshold of asking for help or express their opinion gets lower. Therefore, this is very important in assuring that we get a benefitting collaboration. We are going to support each other by being considerate, uplifting and understanding. 
### Disagreement, breach of contract
If a team member disagrees with the contract, then his matter should be discussed with the team forehand to see if there are any other possible solutions. If the member is not comfortable with discussing it with the full team, then it should be addressed with the project leader who will then address it with the rest of the team after. 

## **Signatures**
[Signatures](https://gitlab.stud.idi.ntnu.no/clmollan/prog1004_2021_group20/-/blob/master/Commitment%20agreement/Signatur.PNG)
