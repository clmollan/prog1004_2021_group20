/**
 *   To-do-list Application, 'Updated'
 *
 *   'Updated' is a time management program that gives
 *   the user the ability to create a personal task. The
 *   application will also make it possible to give the task
 *   a name, put it in a priority, the status of the task, deadline
 *   of the completion, startdate and ID. The user will also
 *   be able to edit the task, or delete the task.
 *
 *   @file ToDoList.CPP
 *   @date 30.04.2021
 *   @authors Alexander Kristian Damahug, Anders Lunde Hagen,
 *            Sander Thorsen, Petar Ilic og Camilla Molland
 */

#include <algorithm>            // find_if
#include <unistd.h>             // usleep
#include <iostream>             // cin, cout
#include <fstream>              // ifstream, ofstream
#include <iomanip>              // setw
#include <cctype>
#include <cstdlib>
#include <string>               // string
#include <sstream>              // split string into int
#include <list>                 // list
#include <vector>               // vector
#include "readData.h"           // Functions for reading int, float and string
using namespace std;

void changePriority();
void deleteTask();
void editTask();
string logInScreen();
void markTaskDone();
string newDate(string date);
void newTask();
void readFromFile(string f);
void taskPriority();
void writeAllPriority();
void writeAllTasks();
void writeMenu();
void writeToFile(string f);


/**
* Task (with name, priority, deadline, startdate,
*       finishdate, category and status)
*/
class Task {
    private:
        int ID;
        int priority;
        bool status;
        string category;
        string deadline;
        string finishDate;
        string name;
        string startDate;
    public:
        //Constructor
        Task(int newID) { name = " "; priority = 1; deadline = " ";
                startDate = " "; category = " "; status = false;
                finishDate = "Task not done!"; ID = newID;}
        //Constructor for reading from file
        Task(ifstream & in, int ID);
        //Functions
        virtual void changePriority();
        virtual void changeStatus();
        virtual void editTask();
        virtual int  getID(){return ID;}
        virtual string getName(){return name;}
        virtual int  getPriority(){return priority;}
        virtual bool getStatus(){return status;}
        virtual void readData();
        virtual void writeData() const;
        virtual void writeToFile(ofstream & out) const;
};

list <Task*> gTasks;            ///< Global 'Task' pointer data-structure
int taskID = 0;                 ///< The ID of the last task created
int userID = 0;                 ///< Amount of users in the database



/**
* Main program, reads and writes to file. Starts the log in process.
*/
int main()
{
    char command;
    string logInUser;
    logInUser = logInScreen(); //Login screen

    readFromFile(logInUser);
    writeMenu();
    command = readChar("\nCommand");

    while (command != 'Q') {
        switch (command) {
        case 'N': newTask(); writeMenu();                    break;
        case 'E': editTask();  writeMenu();                  break;
        case 'D': deleteTask();  writeMenu();                break;
        case 'M': markTaskDone();  writeMenu();              break;
        case 'P': changePriority();  writeMenu();            break;
        case 'L': writeAllPriority(); writeMenu();           break;
        case 'W': writeAllTasks(); writeMenu();             break;
            default:  writeMenu(); writeMenu();              break;
        }
        command = readChar("\nCommand");
    }

    writeToFile(logInUser);

return 0;
}

//==========================================================================
//                            Class members of 'Task'
//==========================================================================


/**
* Constructor for 'Task' when reading from file. Creates new 'Task's with
* information from a user's file
*/
Task::Task(ifstream & in, int ID) {
    this->ID = ID;
    in.ignore();
    getline(in,name);           //Name of task
    getline(in,category);       //Category
    in >> priority; in.ignore();//Priority
    getline(in,deadline);       //Deadline
    getline(in,startDate);      //Starting date
    in >> status;               //Status
    in.ignore();
    if(status == 1){
        status = true;
        getline(in,finishDate); //Finish date
    }else{
        finishDate = "Task not done!";
    }
}

/**
* Lets the user change the priority of a task
*/
void Task::changePriority() {
    char pri;
    bool prioritySet = false;

    while(!prioritySet){
        pri = readChar("What is the new priority of the task?\n"
                            "\t(H)igh, (M)edium, (L)ow");
        switch(pri){
            case 'H': priority = 3; prioritySet = true;    break;
            case 'M': priority = 2; prioritySet = true;    break;
            case 'L': priority = 1; prioritySet = true;    break;
        }
    }
}

/**
* Sets task to done and lets the user set a finish date on the task
*/
void Task::changeStatus() {
    status = true;
    cout << "Finishdate: "; finishDate = newDate("");
}

/**
* Lets user change the contents of a 'Task'
* Name, category, deadline, startdate and priority
*/
void Task::editTask(){
    char c;

    while(c != 'Q'){
        cout << "\nTask " << ID;
        cout << "\n\t(N)ame:\t\t" << name;
        cout << "\n\t(C)ategory:\t" << category;
        cout << "\n\t(D)eadline:\t" << deadline;
        cout << "\n\t(S)tart date:\t" << startDate;
        cout << "\n\t(P)riority:\t";
        switch(priority){
            case 3: cout << "High"; break;
            case 2: cout << "Medium"; break;
            case 1: cout << "Low"; break;
        };
        cout << "\n\n\t(Q)uit to menu\n";

        c = readChar("\n\tWhat part of the task do you want to edit: ");
        cout << "\n\t";
        switch(c){
            case 'N': cout << "New name: "; getline(cin,name);          break;
            case 'C': cout << "New category: "; getline(cin,category);  break;
            case 'D': cout << "New deadline: "; deadline = newDate(""); break;
            case 'S': cout << "New start date: ";
                      startDate = newDate(deadline);                    break;
            case 'P': changePriority(); break;
        }
    }
}

/**
* Reads all datamembers of a 'Task' from user.
*/
void Task::readData() {
    char pri;
    bool prioritySet = false;


    cout << "\n\nEnter a name for the task: ";
    getline(cin, name);

    cout << "Enter a category for the task: "; getline(cin, category);


    while(!prioritySet){
        pri = readChar("What is the priority of the task?\n"
                            "\t(H)igh, (M)edium, (L)ow");
        switch(pri){
            case 'H': priority = 3; prioritySet = true;    break;
            case 'M': priority = 2; prioritySet = true;    break;
            case 'L': priority = 1; prioritySet = true;    break;
        }
    }

    cout << "\nWhen is the deadline (dd/mm/yyyy): "; deadline = newDate("");

    cout << "\nWhen is the starting date (dd/mm/yyyy): ";
    startDate = newDate(deadline); //Start date must be set before deadline
    cout << '\n';
}

/**
* Writes all the information about a 'Task'
*/
void Task::writeData() const {
    cout << "\n\nTask ID: " << ID << '\n';
    cout << "\tTask: " << name << "\n";

    switch(priority){
        case 3 : cout << "\n\tPriority: High\n"; break;
        case 2 : cout << "\n\tPriority: Medium\n"; break;
        case 1 : cout << "\n\tPriority: Low\n"; break;
    }
    cout << "\tCategory: " << category << "\n";
    cout << "\tStatus: ";
    if(status)
        cout << "Done\n\n";
    else
        cout << "Not done\n\n";

    cout << "\tDeadline: " << deadline << "\n";
    cout << "\tStartdate: " << startDate << "\n";
    cout << "\tFinishdate: " << finishDate << "\n\n";

}

/**
* Writes all the information about a 'Task' to a file
*
* @param  out - File that the 'Task's are written to
*/
void Task::writeToFile(ofstream & out) const {
    out << ID << ' ';
    out << name << '\n';
    out << category << '\n';
    out << priority << '\n';
    out << deadline << '\n';
    out << startDate << '\n';
    out << status << '\n';
    if(status == 1){
        out << finishDate << '\n';
    }
}



//==========================================================================
//                              Other class members
//==========================================================================


/**
* Lets the user change the priority of a task
*
* @see  Task::getID()
* @see  Task::changePriority()
*/
void changePriority() {
    int id;
    bool found = false;

    if(gTasks.size() == 0){
        cout << "\nList of tasks is empty!\n";
    }else{
        //Prints out all the existing tasks to help out the user
        cout << '\n';
        for(auto & val : gTasks){
            cout << "\tID: "<< val->getID() << ", name: "
                 << val->getName() << '\n';
        }

        id = readInt("\nID of the task you are changing "
                     "priority of: ",1,taskID);
        //Goes through list of all 'Task's
        for (const auto & val : gTasks){
            if(id == val->getID()){
                val->changePriority();
                found = true;
            }
        }
        if(!found){
            cout << "\nTask not found\n";
        }
    }
}

/**
* Deletes a task, with a confirmation message for the user to confirm deletion
*
* @see  Task::getID()
* @see  Task::getName()
*/
void deleteTask() {
    bool found = false;
    bool doubleCheck = false;
    int id;
    Task* delTask = nullptr; //Extra pointer

    //Prints out all existing tasks to help out the user
    cout << '\n';
    for(auto & val : gTasks){
        cout << "\tID: "<< val->getID() << ", navn: "
             << val->getName() << '\n';
    }
    cout << '\n';
    if(!gTasks.empty()){
        id = readInt("\n\n\tID of the task you "
                      "would like to delete: ", 1, taskID);

        //Goes through list of 'Task's
        for(const auto & val : gTasks){
            if(id==val->getID()){
                found = true;
                cout <<"\tAre you sure you want to delete task " << val->getID()
                     << "? (N/y)";
                if(readChar("") == 'Y'){
                    doubleCheck = true;
                }else{
                    doubleCheck = false;
                }
            }
        }

        //If task has been found and user is sure about deletion, then proceed
        if(found && doubleCheck){
            auto it = find_if(gTasks.begin(), gTasks.end(),
            [id](const auto & val) { return(val->getID() == id); });
            if (it != gTasks.end()){
                delTask = *it;

                //Deletes chosen 'Task' and pointer to 'Task'
                gTasks.erase(it);
                delete delTask;
                cout << "\n\tTask has been deleted\n\n";
            }
        //If task has been found, but user changes their mind
        }else if(found && !doubleCheck){
            cout << "\n\tTask has not been deleted\n\n";
        }else{
            cout << "\n\tTask not found\n\n";
        }
    }else{
        cout << "\n\n\tNo tasks have been added yet\n\n";
    }
}

/**
* Lets the user edit the different parts of a 'Task'
*
* @see  Task::getID()
* @see  Task::getName()
* @see  Task::editTask()
*/
void editTask() {
    int id;
    bool found = false;

    if(gTasks.size() == 0){
        cout << "\nList of tasks is empty!\n";
    }else{
        //Prints out all the existing tasks to help out the user
        cout << '\n';
        for(auto & val : gTasks){
            cout << "\tID: "<< val->getID() << ", name: "
                << val->getName() << '\n';
        }
        cout << '\n';
        id = readInt("\tID of the task you would like to edit", 1, taskID);

        //Goes through list of 'Task's
        for (const auto & val : gTasks){
            if(val->getID() == id){
                val->editTask();
                found = true;
            }
        }
        if(!found){
            cout << "\nTask not found\n";
        }
    }
}

/**
* Presents a log in screen for the user, where user can log in or create a
* new user. All users are given their own personal file of 'Task's
*
* @return Name of personal file of tasks for logged in user
*/
string logInScreen() {
    bool userExist;
    bool passwordCorrect;
    char logIn;
    string user;
    string password;
    string file;
    string fUser;
    string fPassword;
    string fFile;
    string userLine;
    vector <string> userVector;

    //Opens the file to store all the data in a vector for writing to file later
    ifstream infile("USERS.DTA");
    if(infile){
        infile >> userID; infile.ignore();
        if(userID!=0){
            getline(infile,userLine);
            userVector.push_back(userLine);

            while(!infile.eof()){
                getline(infile,userLine);
                userVector.push_back(userLine);
            }
        }
    }else{
        cout << "User database missing\n";
    }
    infile.close();

    //L for login, c for creating new user
    while(!(logIn == 'L' || logIn == 'C')){
        cout << "\n\n\n\tWelcome to Updated!\n\n\t(L)og "
                "in\n\t(C)reate a new user";
        logIn = readChar("\n\n\tCommand");
        system("CLS"); //Clears screen
        userExist = false;
        passwordCorrect = false;


        //Create a new user option
        if(logIn == 'C'){
            cout << "\n\n\n\tCreate a new user on Updated   (case-sensitive)";

            //Check if user already exsits in database
            cout << "\n\n\tUsername:"; getline(cin,user);
            cout << "\tPassword:"; getline(cin,password);

            //Opens up the user database file
            ifstream infile("USERS.DTA");
            if(infile){
                infile >> userID; infile.ignore();
                infile >> fUser; infile.ignore();
                while(!infile.eof()){

                    infile >> fPassword; infile.ignore();
                    infile >> fFile; infile.ignore();

                    //Looks for already existing user in database
                    if(user.compare(fUser) == 0){
                        cout << "\n\tUsername already taken, "
                             "try logging in or change username"
                             << "\n\n\tPress enter to go back...";
                        cin.get();
                        userExist = true;
                        logIn = 'o'; //Cancels user input
                    }
                    infile >> fUser; infile.ignore(); //Checks for another user
                }
            }else{
                cout << "User database missing\n";
            }
            infile.close();

            //If username does not exist, create a new user
            if(!userExist){
                string newUser;
                string newUserFile;

                cout << "\n\tNew user created, welcome " << user << '!';
                userID++;
                newUserFile = "user" + to_string(userID);
                cout << "\n\n\tPress 'enter' to start creating tasks!";
                cin.get();

                //Adds new user to 'userVector' for writing to file later
                newUser = user + " " + password + " " + newUserFile;
                userVector.push_back(newUser);

                //Writes all users from 'userVector' to file
                ofstream outfile("USERS.DTA");
                outfile << userID;

                for (const auto val : userVector){
                    outfile << '\n' << val;
                    cout << val;
                }
                outfile.close();

                system("CLS"); //Clears screen
                return newUserFile;
            }
        }

        //Log in option
        if(logIn == 'L'){
            cout << "\n\n\n\tLog in to Updated    (case-sensitive)";
            cout << "\n\n\tUsername:"; getline(cin,user);
            cout << "\tPassword:"; getline(cin,password);

            //Opens up the user database file
            ifstream infile("USERS.DTA");
            if(infile){
                infile >> userID; infile.ignore();
                infile >> fUser; infile.ignore();
                while(!infile.eof()){

                    infile >> fPassword; infile.ignore();
                    infile >> fFile; infile.ignore();

                    //Looks for existing user in database
                    //if username and password is correct, return user task file
                    if(user.compare(fUser) == 0){
                        if(password.compare(fPassword) == 0){
                            system("CLS"); //Clears screen
                            return fFile;
                        }
                        userExist = true;
                    }
                    infile >> fUser; infile.ignore(); //Checks for another user
                }
            }else{
                cout << "User database missing\n";
            }
            infile.close();

            //If user does not exist, reset input and go back to log in screen
            if(!userExist){
                logIn = 'o'; //Resets user input
                cout << "\n\tUser not found, try again or create a new user.";
                cout << "\n\n\tPress 'enter' to go back...";
                cin.get();
            }else{
                logIn = 'o'; //Resets user input
                cout << "\n\tIncorrect password, try again.";
                cout << "\n\n\tPress 'enter' to go back...";
                cin.get();
            }
        }
        system("CLS"); //Clears screen
    }
}

/**
* Lets the user mark a 'Task' as finished and add a finish date
*
* @see  Task::changeStatus()
* @see  Task::getID()
* @see  Task::getStatus()
* @see  Task::Name()
*/
void markTaskDone() {
    int id;
    bool found = false;

    if(gTasks.size() == 0){
        cout << "\nList of tasks is empty!\n";
    }else{

        //Prints out all the existing tasks to help out the user
        cout << '\n';
        for(auto & val : gTasks){
            if(!val->getStatus()){
                cout << "\tID: "<< val->getID() << ", navn: "
                    << val->getName() << '\n';
            }
        }
        cout << '\n';
        id = readInt("\tID of the task you would like to mark as done", 1, taskID);

        //Goes through all the 'Task's
        for (const auto & val : gTasks){
            if(val->getID() == id){
                if(val->getStatus()){
                    cout << "\n\tTask already finished\n";
                }else{
                    val->changeStatus();
                }
                found = true;
            }
        }
        if(!found){
            cout << "\nTask not found\n";
        }
    }
}

/**
* Lets user input a date in format (dd/mm/yyyy), which is then converted
* and returned as a string "dd/mm/yyyy". If there is another date to take into
* account, param 'date', the user will not be allowed to enter a date which
* comes before the param 'date'.
*
* @param    date  - A deadline the date can not go past or empty if there is
*                no date to take into account
* @return   Returns a string of the date the user entered "dd/mm/yyyy"
*/
string newDate(string date) {
    bool monthValid = false;
    bool yearValid = false;
    bool validDate = false;
    int dd;
    int mm;
    int yyyy;


    //Makes sure date is valid compared to 'date'
    while(!validDate){
        //Reads day
        dd = readInt("\n\tdd",1,31);

        //Reads month based on day
        while(!monthValid){
            monthValid = true;
            mm = readInt("\tmm",1,12);
            if(dd == 31){
                switch(mm){
                    case 2: cout << "\n\tFebruary is not valid";
                                monthValid = false;           break;
                    case 4: cout << "\n\tApril is not valid";
                                monthValid = false;           break;
                    case 6: cout << "\n\tJune is not valid";
                                monthValid = false;           break;
                    case 9: cout << "\n\tSeptember is not valid";
                                monthValid = false;           break;
                    case 11: cout << "\n\tNovember is not valid";
                                monthValid = false;           break;
                }
            }else if(dd == 30){ //Leap year is not taken into account
                if(mm == 2){
                    cout << "February is not valid";
                    monthValid = false;
                }
            }
        }

        //Reads year, if 29/02, it makes sure a leap year is picked
        while(!yearValid){
        yearValid = true;
            yyyy = readInt("\tyyyy",2021,2099);
            if(dd == 29 && mm == 2 && yyyy % 4 != 0){
                cout << yyyy << " is not a leap year";
                yearValid = false;
            }
        }

        //If param 'date' is not empty, convert the string into 3 integers
        if(date.empty()){
            validDate = true;
        }else{
            istringstream is(date);
            int d, m, y;
            is >> d;
            is.ignore();
            is >> m;
            is.ignore();
            is >> y;
            validDate = true;

            //Checks if user input year is greater than param 'date' year
            if(yyyy > y){
                validDate = false;
                monthValid = false;
                yearValid = false;
                cout << "\t\nEntered date is invalid. " << dd << '/'
                     << mm << '/' << yyyy << " must be set before " << date;
            }

            //If it is the same year, check month
            if(yyyy == y){
                //If month is after param 'date' month
                if(mm > m){
                    validDate = false;
                    monthValid = false;
                    yearValid = false;
                    cout << "\t\nEntered date is invalid. " << dd << '/'
                         << mm << '/' << yyyy << " must be set before " << date;
                //If month is the same, check day
                }else if(mm == m){
                    //If day is after param 'date' month
                    if(dd > d){
                        validDate = false;
                        monthValid = false;
                        yearValid = false;
                        cout << "\t\nEntered date is invalid. " << dd << '/'
                             << mm << '/' << yyyy << " must be set before "
                             << date;
                    }
                }
            }
        }
    }

    //Converts all the integers into a dd/mm/yyyy string format
    date = ((dd < 10) ? "0" : "") + to_string(dd)+ "/"
            + ((mm < 10) ? "0" : "") + to_string(mm) + "/"
            + to_string(yyyy);

    return date;
}


/**
* Registers a new task with information from user. Adds the task to 'gTasks'
* and sorts them by ID
*
* @see  Task::readData()
* @see  Task::getID()
*/
void newTask() {
    Task* newTask = nullptr;
    taskID++;
    newTask = new Task(taskID);
    newTask->readData();
    gTasks.push_back(newTask);

    //Sort 'Task's in list by their ID
    gTasks.sort([](Task* t1, Task* t2)
                        { return (t1->getID() < t2->getID()); });
}

/**
* Creates 'Task's from a user's personal 'Task' file.
*
* @param  f - name of the personal file of 'Task's for the user
*/
void readFromFile(string f) {

    int lastID;
    int tID;
    string name;

    f = f+".DTA";
    ifstream infile(f);

    if(infile){
        cout << "Reading from file";
        //Sleeps the program for effect
        unsigned int microsec = 500000;
        for(int i = 0; i < 3; i++){
            cout << ". ";
            usleep(microsec);
        }
        cout << " DONE";
        usleep(500000);
        system("CLS");

        infile >> taskID; infile.ignore();
        infile >> tID;
        while(!infile.eof()){
            Task* task = new Task(infile,tID);
            gTasks.push_back(task);
            infile >> tID; //Check for another task in file
        }
    }else{
        cout << "No existing task database file found\n";
    }
    infile.close();
}



/**
* Prints out all the tasks in descending order from high to low priority
*
* @see  Task::getID()
* @see  Task::getPriority()
* @see  Task::getName()
*/
void writeAllPriority() {

    //Prints all tasks with high priority, then medium, then low
    if(gTasks.size() == 0){
        cout << "\nList of tasks empty!\n";
    }else{
        //High priority
        for (const auto & val : gTasks){
            if(val->getPriority() == 3){
                cout << "\n\tTask ID: " <<val->getID() << '\n';
                cout << "\tName: " <<val->getName() << '\n';
                cout << "\tPriority: High\n";
            }
        }
        //Medium priority
        for (const auto & val : gTasks){
            if(val->getPriority() == 2){
                cout << "\n\tTask ID: " <<val->getID() << '\n';
                cout << "\tName: " <<val->getName() << '\n';
                cout << "\tPriority: Medium\n";
            }
        }
        //Low priority
        for (const auto & val : gTasks){
            if(val->getPriority() == 1){
                cout << "\n\tTask ID: " <<val->getID() << '\n';
                cout << "\tName: " <<val->getName() << '\n';
                cout << "\tPriority: Low\n";
            }
        }
    }
}

/**
* Prints out all the 'Task's for the user
*
* @see  Task::writeData()
*/
void writeAllTasks() {
    if(gTasks.size() == 0){
        cout << "\nList of tasks empty!\n";
    }else{
        for (const auto & val : gTasks){
            val->writeData();
        }
    }
}



/**
 *  Writes menu on screen.
 */
void writeMenu() {
    cout << "\nType inn a commando:\n"
         << "\tN - New task\n"
         << "\tE - Edit task\n"
         << "\tD - Delete task\n"
         << "\tM - Mark task as done\n"
         << "\tP - Change priority of task\n"
         << "\tL - List task in order of priority\n"
         << "\tW - Write all tasks\n"
         << "\tQ - Quit\n";
}

/**
* Writes all the 'Task's from user to their personal 'Task' file.
*
* @see  Task::writeToFile()
* @param  f - Name of the user's personal file
*/
void writeToFile(string f) {
    cout << "Writing to file";

    //Opens the user's personal file
    f = f + ".DTA";
    ofstream outfile(f);

    outfile << taskID << '\n';
    for (const auto & val : gTasks){
        val->writeToFile(outfile);
    }

    outfile.close();

    //Sleeps the program for effect
    unsigned int microsec = 500000;
    for(int i = 0; i < 3; i++){
        cout << ". ";
        usleep(microsec);
    }
    cout << " DONE";
}
