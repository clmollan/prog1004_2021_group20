# Universal Design
Web content accessibility guidelines (WCAG) covers a wide range of recommendations to make content more accessible. This project was designed according to the WCAG 2.1 principle 1 Perceivable. Within this principles there are 4 guidelines, text options, options for time-based media, content can be customized and content is identifiable. 

## 1.1 Text option
The purpose of this guideline is to ensure that all non-text content is available in text. Because our application is accessed though a terminal there are no images or any other tools besides plain text, which means the information could be spoken aloud making it easier for people with reading disabilities to use our to-do-list application. 

## 1.2 Options for time-based media 
Our application does not include any media that is audio-only, video-only or audio-video and therefore doesn’t need any alternatives for time-based media.

## 1.3 Adaptable
Because of the terminals simpleness where you can’t use titles, colours or fonts to indicate that several items are related to each other, its very important to use spacing. Spacing between lines works the same way as these tools do, making it easy for people to understand how the application works without needing different fonts or colours to help. 

## 1.4 Distinguishable 
The purpose of this guideline is to make information as easy to perceive as possible for people with disabilities. People with vision impairments have a much harder time separating foreground and background information. The text in the terminal is  white while the background is black, this contrast makes it easier to read as the information stand out more. In addition we also keep the information as minimum and intuitive as possible by using only short sentences or words. 
