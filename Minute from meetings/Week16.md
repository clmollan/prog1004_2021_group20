# Minute from project meeting for team 20

**Date:** 21/04/21 

**Time:** 1400-1600 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 30** Meeting

Talk to and discuss second iteration with Ali and TA. 


**Case 31** toDoList.cpp

We work on improving our user interactions in toDoList.cpp, limiting some functions making the program more usable

 

# Minute from project meeting for team 20

**Date:** 24/04/21 

**Time:** 1400-1800 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 32** Vision document

We worked with the last feedbacks on the vision document


**Case 33** toDoList.cpp

We highly improved and managed to finish toDoList.cpp


**Case 34** Gannt chart

Gannt Chart was improved according to the feedback we got


**Case 35** Sequence diagram

Finished Sequence diagram and worked with the feedback


**Case 36** Wireframe

Made the wireframe match with the newly improved program


**Case 37** Final report

Started briefly with the main final report


# Minute from project meeting for team 20

**Date:** 28/04/21 

**Time:** 1400-1600 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 38** Meeting

Talk to and discuss final preparations with TA. 




