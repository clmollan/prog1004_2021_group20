# Minute from project meeting for team 20

**Date:** 08/03/21 

**Time:** 1400-1600 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 12** Wiki 

Wiki Uploaded, finished requirement documents after requested Wiki structure


**Case 13** Code

Git programming, started with a layout of the code to make a very simple outline of how our program should look.   




# Minute from project meeting for team 20

**Date:** 10/03/21 

**Time:** 1400-1800 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 14** TA

We asked the TA some questions to clarify what to do further. 


**Case 15** Feedback

Read through the feedback from 1st iteration and talked about what we should do to improve our project.


**Case 16** Diagrams

Finished with Gantt-chart, use case diagram and sequence diagram
