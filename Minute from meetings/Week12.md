# Minute from project meeting for team 20

**Date:** 22/03/21 

**Time:** 1400-1700 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 20** Vision document

We improve and work on the vision document based on our feedback from the 1st iteration.

 

# Minute from project meeting for team 20

**Date:** 24/03/21 

**Time:** 1400-1500 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 21** TA meeting

Questions and feedback meeting with our TA




# Minute from project meeting for team 20

**Date:** 26/03/21 

**Time:** 1200-1400 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 22** Sequence diagram

Continued and finished with the sequence diagram


**Case 23** Wireframe

Updated and improved the wireframe


**Case 24** Vision document

Did some small changes and upgrades on the vision document


**Case 25** Gantt chart

Updated and improved the Gantt chart

