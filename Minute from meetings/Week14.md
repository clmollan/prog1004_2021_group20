# Minute from project meeting for team 20

**Date:** 06/04/21 

**Time:** 1400-1700 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 26** Code

We continue working on the programming. We work on getting some working functions and work on getting a working program



# Minute from project meeting for team 20

**Date:** 07/04/21 

**Time:** 1400-1700 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 27** MVP

We continued working on and finished the MVP


**Case 28** User test document

Finished the user tests and the document


**Case 29** Vision document

Finished the vision document for second iteration

