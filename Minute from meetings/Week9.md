# Minute from project meeting for team 20

**Date:** 01/03/21 

**Time:** 1400-1600 

**Location:** Discord 

**Present:** Sander Thorsen, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** Alexander Damhaug

## Agenda  

**Case 03** Vision document

Starting on the visual document.

Finishing Introduction, Positioning and Project goals.

**Case 04** Planning for meeting with TA

Planning for the meeting with out TA, finding out what is unclear about the assignment and write questions.

*Should we base our wireframe on the terminal window?*

*Should the whole Vision document be finished by Synday 07/03?*

*Are we supposted to work for/be apart of a pretend business?*

**Case 05** BalsamiQ wireframe

Creating a shared BalsamiQ.

*Due to uncertainty about the wireframe design, no further work until meeting with TA.*

 
# Minute from project meeting for team 20

**Date:** 03/03/21 

**Time:** 1400-1600 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic, Camilla Molland and Elvis Arifagic

**Absent:** No one

## Agenda  

**Case 06** Question for TA

Asking questions from **case 04** 

Getting helpful tips

**Case 07** Vision document

Finishing most of Stakeholders and User Descriptions, Product Overview, Product Features, Constraints, Precedence and Priority.

**Case 08** wireframe BalsamiQ 

Getting started on the wireframe, that is going to be as realisitc as possible meaning that it is going to imitate a terminal window.


# Minute from project meeting for team 20

**Date:** 05/03/21 

**Time:** 1400-1630 

**Location:** Discord 

**Present:** Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland

**Absent:** No one

## Agenda  

**Case 09** Vision document

Finished first draft of the vision document


**Case 10** Domain model

Finished domain model


**Case 11** BalsamiQ wireframe 

Finished wireframe


 


 
