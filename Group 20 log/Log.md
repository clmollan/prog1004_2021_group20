# Group 20 log

## Week 8

**Wednesday:**

This week we made a group contract and signed it. Everyone worked together to get it as we wanted the contract.
## Week 9

**Monday:**

We started working on our first iteration. We started working on the vision document and touched upon the wireframe and domain model to get a rough idea of what needed to be handed in. 

**Wednesday:**

We had a meeting with our TA and started with the Vision document. At the end of the session, we had made good progress on the vision document and finished the 1st iteration wireframe in balsamiq. We planned a new session on Friday, where we hope to finish the first iteration of the vision document and domain model, so that we may begin user testing.   
## Week 10

**Monday:**

Uploaded all the requirement documents, such as domain model, wireframe user test and vision document after the specified Wiki structure. We then started on the second iteration which is mostly based on the MVP and created a C++ file in GitLab. Together we created a skeleton program that is simulating the final program. 

**Wednesday:**

Had a meeting with our TA and got feedback from the work done at first iteration. Started and finished Gantt chart and User case diagram. We also started with Sequence diagram for login.
## Week 11

**Monday:**

Updated our vision document after feedback from our TA. 


**Wednesday:**

Had a meeting with Ali and TA.

**Friday:**

Continued with the updated vision document version after feedback from our TA, but we encountered some problems with pull, commit and push with GitLab. 
## Week 12

**Monday:**

We all continued working on the vision document, adding and editing paragraphs.

**Wednesday:**

Meeting with TA. Continued on the vision document. We added some basic functions in toDoList.cpp to get the basics down for later.
## Week 14

**Tuesday:**

We finished up the vision document with some finishing touches. We also worked on our application and got toDoList.cpp to work close to our MVP. We added newTask, deleteTask, writeAllTasks. As well as some helping functions like readInt and readChar.

**Wednesday:**

Meeting with TA. Final meeeting before 2nd iteration. Finished the application toDoList.cpp. Finished all the documents so they are ready for hand-in. 
## Week 15

**Wednesday:**

Meeting with TA and got some feedback on the second iteration hand-in.

## Week 16


**Wednesday:**

Second iteration meeting with Ali and TA. Discussed the MVP and got some feedback.

**Saturday:**


