#ifndef __readDATA_H
#define __readDATA_H


#include <iostream>            //  cin, cout
#include <iomanip>             //  setprecision
#include <cctype>              //  toupper
#include <cstdlib>             //  atoi, atof

const int  MAXCHAR = 200;

char  readChar(const char* t);
float readFloat(const char* t, const float min, const float max);
int   readInt(const char* t, const int min, const int max);



char readChar(const char* t)  {
     char character;
     std::cout << t << ":  ";
     std::cin >> character;  std::cin.ignore(MAXCHAR, '\n');
     return (toupper(character));
}

float readFloat (const char* t, const float min, const float max)  {
    char buffer[MAXCHAR] = "";
    float number = 0.0F;
    bool  wrong = false;

    do {
        wrong = false;
        std::cout << std::fixed << std::showpoint << std::setprecision(2);
        std::cout << t << " (" << min << " - " << max << "):  ";
        std::cin.getline(buffer, MAXCHAR);
        number = static_cast <float> (atof(buffer));
        if (number == 0 && buffer[0] != '0')
           {  wrong = true;   std::cout << "\nERROR: Not a float\n\n";  }
    } while (wrong ||  number < min  ||  number > max);

    return number;
}

int readInt(const char* t, const int min, const int max)  {
    char buffer[MAXCHAR] = "";
    int  number = 0;
    bool wrong = false;

    do {
        wrong = false;
        std::cout << t << " (" << min << " - " << max << "):  ";
        std::cin.getline(buffer, MAXCHAR);
        number = atoi(buffer);
        if (number == 0 && buffer[0] != '0')
        {  wrong = true;   std::cout << "\nERROR: Not an integer\n\n";  }
    } while (wrong  ||  number < min  ||  number > max);

    return number;
}

#endif
