/**
 *   To-do-list Application
 *
 *   Make an application that gives the user the ability
 *   to create a personal task. The application will also
 *   make it possible to give the task a name, put it in
 *   a priority, the status of the task, deadline of the
 *   completion, startdate and ID. The user will also
 *   be able to edit the task, or in other cases delete
 *   the task.
 *
 *   @file ToDoList.CPP
 *   @date
 *   @authors Alexander Kristian Damahug, Anders Lunde Hagen,
 *            Sander Thorsen, Petar Ilic og Camilla Molland
 */


#include <unistd.h>
#include <iostream>              // cin, cout
#include <fstream>               // ifstream, ofstream
#include <iomanip>              // setw
#include <cctype>
#include <cstdlib>
#include <string>               // string
#include <map>                  // map
#include "readData.h"           // Functions for reading ints, floats and strings
using namespace std;


void newTask();
void deleteTask();
void editTask();
void markTaskDone();
void taskPriority();
void writeAllTasks();
void writeToFile();
void readFromFile();
void writeMenu();

/**
* Task (with name, priority, deadline, startdate, finishdate, category and status)
*/
class Task {
    private:
        string name;
        int priority;
        string deadline;
        string startdate;
        string finishdate;
        string category;
        bool status;
    public:
        Task() { name = " "; priority = 0; deadline = " "; startdate = " "; category = " "; status = false; finishdate = "Task not done!"; }
        Task(ifstream & in);
        virtual void changeStatus(); //= 0;
        virtual void readData();
        virtual void writeData() const;
        virtual void writeToFile(ofstream & out) const;
        virtual int getPriority(){return priority;}
        virtual void changePriority(int p) { priority = p; }

};


map <int, Task*> gTasks;           ///< All tasks

/**
* Main program
*/
int main()
{
    char command;

    readFromFile();
    writeMenu();
    command = readChar("\nCommand");

    while (command != 'Q') {
        switch (command) {
        case 'N': newTask();                     break;
        case 'E': editTask();
        case 'D': deleteTask();                  break;
        case 'M': markTaskDone();                break;
//        case 'P': Change priority of task;     break;
//        case 'L': List task order;             break;
        case 'W': writeAllTasks();               break;
            default:  writeMenu();               break;
        }
        command = readChar("\nCommand");
    }
writeToFile();

return 0;
}

/**
*
*/
Task::Task(ifstream & in) {

    in >> name >> priority >> deadline;
    in.ignore();
    //this->name = name;
}

/**
* Reads all datamembers of a Task
* @param name - name of the task
* @param priority - prioirty of the task
* @param category - category of the task
* @param deadline - deadline of the task
* @param startdate -startdate of the task
*/
void Task::readData() {

    int input;
    bool priorityLoop = true;
    int tempPriority;

    cout << "\nThe name of the task: ";
    getline(cin, name);

    //Checks if priority is taken
    while(priorityLoop){
        priorityLoop = false;
        input = readInt("What priority is this task?", 1, gTasks.size() + 1);

        for (const auto& val : gTasks) {
            tempPriority = val.second->getPriority();
            if (tempPriority == input){
                cout << "A task already has this priority, "
                << "set another priority.\n";
                priorityLoop = true;
            }
        }
    }
    priority = input;

    cout << "Category: "; getline(cin, category);

    cout << "\nDeadline: "; getline(cin, deadline);
    cout << "Startdate: "; getline(cin, startdate);


}

/**
* Outputs all the datamembers of the task
* @param name - name of the task
* @param priority - prioirty of the task
* @param category - category of the task
* @param deadline - deadline of the task
* @param startdate -startdate of the task
*/
void Task::writeData() const {
    cout << "The name of the task: " << name << "\n";
    cout << "Priority: " << priority << "\n";
    cout << "Category: " << category << "\n\n";

    cout << "Status: ";
    if(status)
        cout << "Done\n\n";
    else
        cout << "Not done\n\n";

    cout << "Deadline: " << deadline << "\n";
    cout << "Startdate: " << startdate << "\n";
    cout << "Finishdate: " << finishdate << "\n";

}

/**
* Writes to file
*/
void Task::writeToFile(ofstream & out) const {
    out << this->name << "\n";
}

/**
* Sets task to done and sets the finishdate
* @param status - the datamember that gets changed to done
* @param finishdate - date of when the task was set to done
*/
void Task::changeStatus() {
    status = true;
    cout << "Finishdate: "; getline(cin, finishdate);
}

/**
* A new task is registered
* @param nr - the ID of the task
* @see addtask::readData()
*/
void newTask() {
    Task* addTask = nullptr;          //  Pointer for new task

    addTask = new Task();

    int nr = readInt("ID of the new task", 0, 999999);

    if (gTasks.find(nr) != gTasks.end()) {
        cout << "This ID already exists!\n";
    } else {
        addTask->readData();
        gTasks.insert(pair <int, Task*> (nr, addTask));
    }
}

/**
* Edit an existing task
*/
void editTask() {

}


/**
* Marks the task as done
* @param id - the ID of the task
* @see Task::changeStatus()
*/

void markTaskDone() {
    int id;

    if (!gTasks.empty()) {

        id = readInt("\tID of the task you would like to mark as done", gTasks.begin()->first, gTasks.rbegin()->first);
        auto it = gTasks.find(id);

        if (it != gTasks.end()) {
            cout << "\n\tMarking the task as done:\n";
            (it->second)->changeStatus();
        }
    }else
        cout << "\nNo tasks to mark as done.\n";
}

/**
* Deletes a task
* @param id - the ID of a specific task
* @see Task::writeData()
*/
void deleteTask() {
    int id;

    if (!gTasks.empty()) {

        id = readInt("\tID of the task you would like to delete", gTasks.begin()->first, gTasks.rbegin()->first);
        auto it = gTasks.find(id);

        if (it != gTasks.end()) {

           cout << "\n\tDeleting task\n";

           (it->second)->writeData();
           delete it->second;
           gTasks.erase(it);
        } else
          cout << "\n\tNo tasks with that ID!\n";
    } else
       cout << "\n\tNo registered tasks!\n\n";
}

/**
* TRENGER VI DENNE??
*/
void writeToFile() {
    cout << "Writing to file";

    unsigned int microsec = 500000;
    for(int i = 0; i < 3; i++){
        cout << ". ";
        usleep(microsec);
    }
    cout << " DONE";
}

/**
* Read from file
*/
void readFromFile() {
    cout << "Reading from file";
}

/**
* Outputs all registered tasks
* @see Task::writeData()
*/
void writeAllTasks() {
    if (!gTasks.empty()) {
    cout << "\nCurrent amount of tasks: " << gTasks.size() << '\n';

    cout << "\nData about all tasks\n\n";
    for (const auto& val : gTasks) {
        cout << "ID: " << val.first << '\n';        //  Range-based for-løkke:
        (val.second)->writeData();                  //  Skriver data
        }
    } else
        cout << "\nNo tasks added!\n";
}

/**
 *  Writes menu on screen.
 */
void writeMenu() {
    cout << "\nType inn a commando:\n"
         << "\tN - New task\n"
         << "\tE - Edit task\n"
         << "\tD - Delete task\n"
         << "\tM - Mark task as done\n"
         << "\tP - Change priority of task\n"
         << "\tL - List task order\n"
         << "\tW - Write all tasks\n"
         << "\tQ - Quit\n";
}

