# Meeting summon: Project team 20 

**Date:** 01/03/21 

**Time:** 1400-1600 

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland*

 

## Agenda  

**Case 03** Vision document 

**Case 04** Planning for meeting with TA 

**Case 05** BalsamiQ wireframe  

 

 

# Meeting summon: Project team 20 

**Date:** 03/03/21 

**Time:** 1400-1600 

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic, Camilla Molland and Elvis Arifagic*

 

## Agenda  

**Case 06** Questions for TA 

**Case 07** Vision document 

**Case 08** BalsamiQ wirerame 

 

 

 

# Meeting summon: Project team 20 

**Date:** 05/03/21 

**Time:** 1400-1600 

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Illic and Camilla Molland*

 

## Agenda  

**Case 09** Vision document 

**Case 10** Domain Model 

**Case 11** BalsamiQ wireframe 
