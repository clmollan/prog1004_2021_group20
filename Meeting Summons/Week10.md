# Meeting summon: Project team 20 

**Date:** 08/03/21 

**Time:** 1400-1600 

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Ilic and Camilla Molland*

 

## Agenda  

**Case 12** - Start with making the Wiki on GitLab

**Case 13** - Starting with the programming for an early prototype (MVP)

 

 

# Meeting summon: Project team 20 

**Date:** 10/03/21 

**Time:** 1400-1800 

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Ilic, Camilla Molland and Elvis Arifagic*

 

## Agenda  

**Case 14** - Questions for TA 

**Case 15** - First iteration feedback

**Case 16** - Start/finish with Gantt-chart, use case diagram and sequence diagram



 

 
