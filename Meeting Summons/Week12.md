# Meeting summon: Project team 20 

**Date:** 22/03/21 

**Time:** 1400-1700 

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Ilic and Camilla Molland*



## Agenda  

**Case 20** - Continue working with the Vision Document





# Meeting summon: Project team 20 

**Date:** 24/03/21 

**Time:** 1400-1500 

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Ilic and Camilla Molland*



## Agenda  

**Case 21** - Q/A and Feedback with Elvis




# Meeting summon: Project team 20 

**Date:** 26/03/21 

**Time:** 1200-1400 

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Ilic and Camilla Molland*



## Agenda  

**Case 22** - Working with Sequence Diagram

**Case 23** - Working with Wireframe

**Case 24** - Small fixes with Vision Document

**Case 25** - Improved Gantt Chart



