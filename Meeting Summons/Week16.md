# Meeting summon: Project team 20 

**Date:** 21/04/21 

**Time:** 1400-1600

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Ilic and Camilla Molland*



## Agenda  

**Case 30** - Meeting with Ali and TA

**Case 31** - Work on feedback from meeting, improve toDoList.cpp



# Meeting summon: Project team 20 

**Date:** 24/04/21 

**Time:** 1200-1800

**Location:** Discord 

The following persons are called for:  

*Sander Thorsen, Alexander Damhaug, Anders Lunde Hagen, Petar Ilic and Camilla Molland*



## Agenda  

**Case 32** - Finish Vision Document

**Case 33** - Finish ToDoList.cpp

**Case 34** - Fix Gantt Chart

**Case 35** - Finish Sequence-Diagram

**Case 36** - Finish Wireframe

**Case 37** - Started briefly with the main report
